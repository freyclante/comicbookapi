﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace ComicBookApi.Migrations
{
    public partial class movetosqlserver : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "ComicCollections",
                columns: table => new
                {
                    Id = table.Column<string>(nullable: false),
                    OwnerId = table.Column<string>(nullable: true),
                    Created = table.Column<DateTime>(nullable: false),
                    IsPubliclySearchable = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ComicCollections", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Comics",
                columns: table => new
                {
                    Id = table.Column<string>(nullable: false),
                    Isbn = table.Column<string>(nullable: true),
                    Author = table.Column<string>(nullable: true),
                    ComicBookArtist = table.Column<string>(nullable: true),
                    Publisher = table.Column<string>(nullable: true),
                    SeriesName = table.Column<string>(nullable: true),
                    State = table.Column<string>(nullable: true),
                    Title = table.Column<string>(nullable: true),
                    Edition = table.Column<int>(nullable: true),
                    Issue = table.Column<int>(nullable: true),
                    Value = table.Column<double>(nullable: true),
                    CoverType = table.Column<string>(nullable: true),
                    ProductionYear = table.Column<int>(nullable: true),
                    Remarks = table.Column<string>(nullable: true),
                    ForSale = table.Column<bool>(nullable: false),
                    CollectionId = table.Column<string>(nullable: true),
                    ComicCollectionId1 = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Comics", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Comics_ComicCollections_CollectionId",
                        column: x => x.CollectionId,
                        principalTable: "ComicCollections",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Comics_ComicCollections_ComicCollectionId1",
                        column: x => x.ComicCollectionId1,
                        principalTable: "ComicCollections",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ComicPhotos",
                columns: table => new
                {
                    Id = table.Column<string>(nullable: false),
                    Uri = table.Column<string>(nullable: true),
                    Remarks = table.Column<string>(nullable: true),
                    ComicBookId = table.Column<string>(nullable: true),
                    ComicBookId1 = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ComicPhotos", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ComicPhotos_Comics_ComicBookId",
                        column: x => x.ComicBookId,
                        principalTable: "Comics",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ComicPhotos_Comics_ComicBookId1",
                        column: x => x.ComicBookId1,
                        principalTable: "Comics",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_ComicPhotos_ComicBookId",
                table: "ComicPhotos",
                column: "ComicBookId");

            migrationBuilder.CreateIndex(
                name: "IX_ComicPhotos_ComicBookId1",
                table: "ComicPhotos",
                column: "ComicBookId1");

            migrationBuilder.CreateIndex(
                name: "IX_Comics_CollectionId",
                table: "Comics",
                column: "CollectionId");

            migrationBuilder.CreateIndex(
                name: "IX_Comics_ComicCollectionId1",
                table: "Comics",
                column: "ComicCollectionId1");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ComicPhotos");

            migrationBuilder.DropTable(
                name: "Comics");

            migrationBuilder.DropTable(
                name: "ComicCollections");
        }
    }
}
