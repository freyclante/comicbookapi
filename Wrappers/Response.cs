using Newtonsoft.Json;

namespace ComicBookApi.Wrappers
{
    public class Response<T>
    {
        [JsonProperty(PropertyName = "message")]
        public string Message { get; set; }
        [JsonProperty(PropertyName = "errors")]
        public string[] Errors { get; set; }
        [JsonProperty(PropertyName = "data")]
        public T Data { get; set; }
        
        public Response() {}

        public Response(T data)
        {
            Message = string.Empty;
            Errors = null;
            Data = data;
        }
    }
}