using ComicBookApi.Data.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace ComicBookApi.Configuration
{
    public class AppUserConfiguration : IEntityTypeConfiguration<MyPreciousComicsUser>
    {
        public void Configure(EntityTypeBuilder<MyPreciousComicsUser> builder) 
        {
            builder.HasKey(x => x.Id);
        }
    }
}