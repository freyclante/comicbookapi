using System.Security.Claims;
using System.Threading.Tasks;
using ComicBookApi.Authentication.Models;
using ComicBookApi.Data.Entities;
using Microsoft.AspNetCore.Identity;

namespace ComicBookApi.Services
{
    public interface IUserService
    {
        Task<MyPreciousComicsUser> AuthenticateGoogleUserAsync(GoogleUserRequest request);
        Task<IdentityUser> GetUser();
        void RegisterUserWithCollection(string collectionId, string userId);
        Task<bool> AssignRoleToUser(string role);
        Task<bool> CheckUserIsInRole(string role);
        Task<bool> RemoveRoleFromUser(string role);

        Task<bool> DeauthenticateUser();
    }
}