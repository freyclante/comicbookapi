using System.Collections.Generic;
using System.Threading.Tasks;
using ComicBookApi.Data.Entities.Comics;
using ComicBookApi.Filters;
using ComicBookApi.Models;
using ComicBookApi.Wrappers;

namespace ComicBookApi.Services
{
    public interface ICosmosDbService<T>
    {
        Task<PagedResponse<IEnumerable<ComicBook>>> GetItemsAsync(string collectionId, PaginationFilter filter,
            IUriService uriService, string route);
        Task<T> GetItemAsync(string collectionGuid, string comicGuid);
        Task AddItemAsync(string collectionId, T item);

        Task UpdateItemAsync(string collectionGuid, T item);
        Task DeleteItemAsync(string collectionGuid, string comicGuid);
    }
}