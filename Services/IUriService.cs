using System;
using ComicBookApi.Filters;

namespace ComicBookApi.Services
{
    public interface IUriService
    {
        public Uri GetPageUri(PaginationFilter filter, string route);
    }
}