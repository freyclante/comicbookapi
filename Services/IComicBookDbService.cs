using System.Collections.Generic;
using System.Threading.Tasks;
using ComicBookApi.Data.Entities.Comics;
using ComicBookApi.Data.Entities.Products;

namespace ComicBookApi.Services
{
    public interface IComicBookDbService : ICosmosDbService<ComicBook>
    {
        Task<IEnumerable<ComicBook>> GetComicsAsyncBySeriesName(string seriesName);
        Task<IEnumerable<ComicBook>> GetComicAsyncBySeriesIssue(string seriesName, int issue);

        Task<ComicCollection> CreateComicsCollectionAsync(IUserService userService);

        Task<IEnumerable<Product>> GetAllProducts();
    }
}