using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using ComicBookApi.Data;
using ComicBookApi.Data.Entities.Comics;
using ComicBookApi.Data.Entities.Products;
using ComicBookApi.Filters;
using ComicBookApi.Helpers;
using ComicBookApi.Wrappers;
using Microsoft.AspNetCore.Identity;
using Microsoft.Azure.Cosmos;
using Microsoft.Azure.Cosmos.Linq;
using Microsoft.EntityFrameworkCore;

namespace ComicBookApi.Services
{
    public class ComicBookDbService : IComicBookDbService
    {
        private readonly AppDbContext _dbContext;

        public ComicBookDbService(AppDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<PagedResponse<IEnumerable<ComicBook>>> GetItemsAsync(string collectionId,
            PaginationFilter filter,
            IUriService uriService, string route)
        {
            try
            {
                var options = new QueryRequestOptions()
                {
                    MaxItemCount = filter.PageSize
                };

                var queryable = _dbContext.ComicCollections.AsQueryable();
                var feedIterator = queryable
                    .Where(c => c.Id.Equals(collectionId))
                    .Select(re => re.Comics).AsQueryable()
                    .Skip((filter.PageNumber - 1) * filter.PageSize)
                    .Take(filter.PageSize)
                    .ToFeedIterator();

                var comics = new List<ComicBook>();

                while (feedIterator.HasMoreResults)
                {
                    var result = await feedIterator.ReadNextAsync();
                    foreach (var comicBooks in result)
                    {
                        comics.AddRange(comicBooks);
                    }
                }

                var totalRecords = queryable.Count();
                return PaginationHelper.CreatePagedResponse(comics, filter, totalRecords, uriService, route);
            }
            catch (Exception e)
            {
                return new PagedResponse<IEnumerable<ComicBook>>(new List<ComicBook>(), new[] {e.Message},
                    "Request failed, see errors array");
            }
        }
        
        public async Task<IEnumerable<Product>> GetAllProducts()
        {
            return await _dbContext.Products.Include(p => p.PaymentPlan)
                .ToListAsync();
        }

        public async Task<ComicBook> GetItemAsync(string collectionGuid, string comicGuid)
        {
            try
            {
                var comicBook =
                    await _dbContext.ComicBooks.FirstOrDefaultAsync(c => c.Id.Equals(comicGuid));
                return comicBook;
            }
            catch (CosmosException ex) when (ex.StatusCode == HttpStatusCode.NotFound)
            {
                return null;
            }
        }

        public async Task AddItemAsync(string collectionId, ComicBook comicBook)
        {
            try
            {
                /*
                var collection = _container
                    .GetItemLinqQueryable<ComicCollection>()
                    .FirstOrDefault(c => c.Id.Equals(collectionId));
                    */

                var collection = await GetComicCollection(collectionId);

                if (collection == null)
                {
                    throw new Exception("No collection found for user!");
                }

                _dbContext.ComicBooks.Add(comicBook);
                await _dbContext.SaveChangesAsync();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        private async Task<ComicCollection> GetComicCollection(string collectionId)
        {
            return await _dbContext.ComicCollections.FirstOrDefaultAsync(col => col.Id.Equals(collectionId));
        }

        public async Task UpdateItemAsync(string collectionId, ComicBook comicBook)
        {
            try
            {
                var collection = await GetComicCollection(collectionId);
                collection.Comics.Add(comicBook);
                await _dbContext.SaveChangesAsync();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        public async Task
            DeleteItemAsync(string collectionGuid,
                string comicGuid) //TODO Un-Nest Comics from ComicCollection to make items deletable.
        {
            try
            {
                var comicBook = await _dbContext.ComicBooks.FirstOrDefaultAsync(cb => cb.Id.Equals(comicGuid));
                _dbContext.ComicBooks.Remove(comicBook);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        public async Task<IEnumerable<ComicBook>> GetComicsAsyncBySeriesName(string seriesName)
        {
            return await _dbContext.ComicBooks
                .Where(cb => cb.SeriesName.Equals(seriesName))
                .ToListAsync();
        }

        public async Task<IEnumerable<ComicBook>> GetComicAsyncBySeriesIssue(string seriesName, int issue)
        {
            return await _dbContext.ComicBooks
                .Where(cb => cb.SeriesName.Equals(seriesName))
                .Where(cb => cb.Issue == issue)
                .ToListAsync();
        }

        public async Task<ComicCollection> CreateComicsCollectionAsync(IUserService userService)
        {
            try
            {
                var user = userService.GetUser().Result;
                var collection = CreateComicsCollection(user);
                var result = await _dbContext.AddAsync(collection);
                await _dbContext.SaveChangesAsync();
                var success = result != null;

                if (success)
                {
                    userService.RegisterUserWithCollection(collection.Id, user.Id);
                    return collection;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }

            return null;
        }

        private static ComicCollection CreateComicsCollection(IdentityUser user)
        {
            return new ComicCollection()
            {
                Id = Guid.NewGuid().ToString(),
                Created = DateTime.Now,
                OwnerId = user.Id,
                IsPubliclySearchable = false,
                Comics = new List<ComicBook>()
            };
        }
    }
}