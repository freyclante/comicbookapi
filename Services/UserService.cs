using System;
using System.Linq;
using System.Threading.Tasks;
using ComicBookApi.Authentication.Models;
using ComicBookApi.Data.Entities;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using static Google.Apis.Auth.GoogleJsonWebSignature;

namespace ComicBookApi.Services
{
    public class UserService : IUserService
    {
        private readonly UserManager<MyPreciousComicsUser> _userManager;
        private readonly IConfiguration _configuration;
        private readonly IHttpContextAccessor _contextAccessor;
        private readonly ILogger<UserService> _logger;

        public UserService(UserManager<MyPreciousComicsUser> userManager, IConfiguration configuration,
            IHttpContextAccessor contextAccessor, ILogger<UserService> logger)
        {
            _userManager = userManager;
            _configuration = configuration;
            _contextAccessor = contextAccessor;
            _logger = logger;
        }

        public async Task<MyPreciousComicsUser> AuthenticateGoogleUserAsync(GoogleUserRequest request)
        {
            Payload payload = await ValidateAsync(request.IdToken, new ValidationSettings
            {
                Audience = new[] { _configuration["Google:ClientId"] }
            });

            var user = await GetOrCreateExternalLoginUser(GoogleUserRequest.Provider, payload.Subject, payload.Email, payload.GivenName, payload.FamilyName);
            return user;
        }

        public async Task<IdentityUser> GetUser()
        {
            return await _userManager.GetUserAsync(_contextAccessor.HttpContext.User);
        }

        public async void RegisterUserWithCollection(string collectionId, string userId)
        {
            try
            {
                var user = await GetUser() as MyPreciousComicsUser;
                if (user == null) return;
            
                user.ComicCollectionGuid = collectionId;
                var result = await Task.FromResult(_userManager.UpdateAsync(user));
                if (result.Result.Succeeded)
                {
                    _logger.Log(LogLevel.Information, "Collection assigned to user");
                    return;
                }
                _logger.Log(LogLevel.Error, "Some error happened when attempting to assign collection-guid to user.");
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        public async Task<bool> AssignRoleToUser(string role)
        {
            var user = await GetUser();
            var result = await _userManager.AddToRoleAsync((MyPreciousComicsUser) user, role);
            return result.Succeeded;
        }

        public async Task<bool> CheckUserIsInRole(string role)
        {
            var user = await GetUser();
           return await _userManager.IsInRoleAsync((MyPreciousComicsUser) user, role);
        }

        public async Task<bool> RemoveRoleFromUser(string role)
        {
            var user = await GetUser();
            var result = await _userManager.RemoveFromRoleAsync((MyPreciousComicsUser) user, role);
            return result.Succeeded;
        }

        public async Task<bool> DeauthenticateUser()
        {
            var user = await GetUser();
            var result =
                await _userManager.RemoveAuthenticationTokenAsync((MyPreciousComicsUser) user, "MyPreciousComicsApp",
                    "Auth");
            return result.Succeeded;
        }

        private async Task<MyPreciousComicsUser> GetOrCreateExternalLoginUser(string provider, string key, string email, string firstName, string lastName)
        {
            MyPreciousComicsUser user = null;
            try
            {
                 user = await _userManager.FindByLoginAsync(provider, key);
            }
            catch(Exception e)
            {
                Console.WriteLine(e.Message);
            }
            if (user != null)
                return user;
            user = await _userManager.FindByEmailAsync(email);
            if (user == null)
            {
                user = new MyPreciousComicsUser()
                {
                    Joined = DateTime.Now,
                    Email = email,
                    UserName = email,
                    FirstName = firstName,
                    LastName = lastName,
                    Id = key,
                };
                await _userManager.CreateAsync(user);
            }

            var info = new UserLoginInfo(provider, key, provider.ToUpperInvariant());
            var result = await _userManager.AddLoginAsync(user, info);
            return result.Succeeded ? user : null;
        }
    }
}