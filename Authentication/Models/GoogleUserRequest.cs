﻿using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;

namespace ComicBookApi.Authentication.Models
{
    public class GoogleUserRequest
    {
        public const string Provider = "google";

        [JsonProperty("idToken")]
        [Required]
        public string IdToken { get; set; }

    }
}