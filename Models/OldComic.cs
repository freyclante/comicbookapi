using Newtonsoft.Json;

namespace ComicBookApi.Models
{
    public class OldComic
    {
        [JsonProperty(PropertyName = "cartoonId")]
        public int cartoonId { get; set; }
        [JsonProperty(PropertyName = "isbn")]
        public string isbn { get; set; }
        [JsonProperty(PropertyName = "author")]
        public string author { get; set; }
        [JsonProperty(PropertyName = "publisher")]
        public string publisher  { get; set; }
        [JsonProperty(PropertyName = "seriesName")]
        public string seriesName { get; set; }
        [JsonProperty(PropertyName = "state")]
        public string state { get; set; }
        [JsonProperty(PropertyName = "title")]
        public string title { get; set; }
        [JsonProperty(PropertyName = "edition")]
        public int? edition { get; set; }
        [JsonProperty(PropertyName = "issue")]
        public int? issue { get; set; }
        [JsonProperty(PropertyName = "value")]
        public int? value { get; set; }
        [JsonProperty(PropertyName = "coverType")]
        public string coverType { get; set; }
        [JsonProperty(PropertyName = "productionYear")]
        public int? productionYear { get; set; }
        [JsonProperty(PropertyName = "remarks")]
        public string remarks { get; set; }
        [JsonProperty(PropertyName = "thumbNailImage")]
        public byte[] thumbNailImage { get; set; }
    }
}