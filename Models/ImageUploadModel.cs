using Microsoft.AspNetCore.Http;

namespace ComicBookApi.Models
{
    public class ImageUploadModel
    {
        public IFormFile data { get; set; }
        public string Guid { get; set; }
        public string remarks { get; set; }
    }
}