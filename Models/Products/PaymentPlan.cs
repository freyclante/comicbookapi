using System.Text.Json.Serialization;
using Newtonsoft.Json;

namespace ComicBookApi.Models.Products
{
    public class PaymentPlan
    {
        public bool IsRecurring { get; set; }
        public string Frequency { get; set; }
    }
}