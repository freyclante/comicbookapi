namespace ComicBookApi.Models.Products
{
    public class PaymentFrequency
    {
        public static readonly PaymentFrequency Onetime = new PaymentFrequency("OneTime");
        public static readonly PaymentFrequency Weekly = new PaymentFrequency("Weekly");
        public static readonly PaymentFrequency Monthly = new PaymentFrequency("Monthly");
        public static readonly PaymentFrequency Yearly = new PaymentFrequency("Yearly");

        public PaymentFrequency(string value)
        {
            Value = value;
        }
        public string Value { get; }
    }
}