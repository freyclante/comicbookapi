namespace ComicBookApi.Models.Products
{
    public class ProductDto
    {
        public string Name { get; set; }
        public bool IsFree { get; set; }
        public double? Price { get; set; }
        public  string Description { get; set; }
        public PaymentPlan PaymentPlan { get; set; }
    }
}