using System.Collections.Generic;
using ComicBookApi.Data.Entities.Products;
using ComicBookApi.Models.Products;
using PaymentPlan = ComicBookApi.Models.Products.PaymentPlan;

namespace ComicBookApi.Mappers
{
    public class ProductMapper
    {
        public static IEnumerable<ProductDto> MapFrom(IEnumerable<Product> products)
        {
            var productDtos = new List<ProductDto>();
            foreach (var product in products)
            {
                var dto = new ProductDto()
                {
                    Name = product.Name,
                    Description = product.Description,
                    IsFree = product.IsFree,
                    Price = product.Price,
                    PaymentPlan = new PaymentPlan()
                    {
                        Frequency = product.PaymentPlan.Frequency,
                        IsRecurring = product.PaymentPlan.IsRecurring
                    }
                };

                productDtos.Add(dto);
            }

            return productDtos;
        }
    }
}