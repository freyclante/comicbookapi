﻿﻿using System;
 using System.Globalization;
 using System.IdentityModel.Tokens.Jwt;
 using System.Linq;
 using System.Security.Claims;
 using System.Text;
 using System.Threading.Tasks;
 using ComicBookApi.Authentication.Models;
 using ComicBookApi.Data.Entities;
 using ComicBookApi.Exceptions;
 using ComicBookApi.Services;
 using Microsoft.AspNetCore.Authorization;
 using Microsoft.AspNetCore.Identity;
 using Microsoft.AspNetCore.Mvc;
 using Microsoft.Extensions.Configuration;
 using Microsoft.IdentityModel.Tokens;
 namespace ComicBookApi.Controllers
{
    [Authorize]
    [ApiController]
    [Route("[controller]")]
    public class AuthController : BaseController
    {
        private readonly SignInManager<MyPreciousComicsUser> _signInManager;
        private readonly IUserService _userService;
        private readonly IConfiguration _configuration;

        public AuthController(UserManager<MyPreciousComicsUser> userManager, SignInManager<MyPreciousComicsUser> signInManager,
            IUserService userService, IConfiguration configuration) : base(userManager)
        {
            _signInManager = signInManager;
            _userService = userService;
            _configuration = configuration;
        }

        [AllowAnonymous]
        [HttpPost("googleauthenticate")]
        public async Task<IActionResult> GoogleAuthenticate([FromBody] GoogleUserRequest request)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState.Values.SelectMany(it => it.Errors).Select(it => it.ErrorMessage));

            return Ok(GenerateUserToken(await _userService.AuthenticateGoogleUserAsync(request)));
        }

        [AllowAnonymous]
        [HttpGet("roles")]
        public async Task<IActionResult> CheckAdminRole([FromQuery] string role)
        {
            try
            {
                var result = await _userService.CheckUserIsInRole(role);
                return new OkObjectResult(result);
            }
            catch (Exception e)
            {
               var err = new ErrorResponse()
                {
                    Message = e.Message,
                    Status = 500
                };
               
               return new ObjectResult(err);
            }
        }

        [HttpPost("roles/add/{role}")]
        public async Task<IActionResult> AddUserToRole([FromBody] string role)
        {
            try
            {
                var result = await _userService.AssignRoleToUser(role);
                return new OkObjectResult(result);
            }
            catch (Exception e)
            {
                var err = new ErrorResponse()
                {
                    Message = e.Message,
                    Status = 500
                };
               
                return new ObjectResult(err);
            }
        }
        
        [HttpPost("roles/remove/{role}")]
        public async Task<IActionResult> RemoveUserFromRole([FromBody] string role)
        {
            try
            {
                var result = await _userService.RemoveRoleFromUser(role);
                return new OkObjectResult(result);
            }
            catch (Exception e)
            {
                var err = new ErrorResponse()
                {
                    Message = e.Message,
                    Status = 500
                };
               
                return new ObjectResult(err);
            }
        }

        [AllowAnonymous]
        [HttpPost("deauthenticate")]
        public async Task<IActionResult> DeAuthenticateUser()
        {
            try
            {
                await _signInManager.SignOutAsync();
                return new ObjectResult(_signInManager.IsSignedIn(User));
            }
            catch (Exception e)
            {
                var err = new ErrorResponse()
                {
                    Message = e.Message,
                    Status = 500
                };
               
                return new ObjectResult(err);
            }
        }
        #region Private Methods
        private UserToken GenerateUserToken(MyPreciousComicsUser user)
        {
            var tokenHandler = new JwtSecurityTokenHandler();

            var key = Encoding.ASCII.GetBytes(_configuration["Authentication:Jwt:Secret"]);

            var expires = DateTime.UtcNow.AddHours(1);
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new[]
                {
                    new Claim(JwtRegisteredClaimNames.Sub, user.Id),
                    new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                    new Claim(JwtRegisteredClaimNames.Iat, DateTime.UtcNow.ToString(CultureInfo.CurrentCulture)),
                    new Claim(ClaimTypes.Surname, user.LastName),
                    new Claim(ClaimTypes.GivenName, user.FirstName),
                    new Claim(ClaimTypes.NameIdentifier, user.UserName),
                    new Claim(ClaimTypes.Email, user.Email)
                }),

                Expires = expires,

                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature),
                Issuer = _configuration["Authentication:Jwt:Issuer"],
                Audience = _configuration["Authentication:Jwt:Audience"],
            };

            var securityToken = tokenHandler.CreateToken(tokenDescriptor);

            var token = tokenHandler.WriteToken(securityToken);

            return new UserToken
            {
                UserId = user.Id,
                Email = user.Email,
                FirstName = user.FirstName,
                LastName = user.LastName,
                Token = token,
                Expires = expires
            };
        }
        #endregion
        
        
    }
}