﻿using System;
using System.Collections.Generic;
using System.Net.Mime;
using System.Text.Json;
using System.Threading.Tasks;
using ComicBookApi.Data.Entities;
using ComicBookApi.Data.Entities.Comics;
using ComicBookApi.Exceptions;
using ComicBookApi.Filters;
using ComicBookApi.Models;
using ComicBookApi.Services;
using ComicBookApi.Wrappers;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;


namespace ComicBookApi.Controllers
{
    [Authorize]
    [ApiController]
    [Route("[controller]")]
    public class ComicBooksController : ControllerBase
    {
        private readonly IComicBookDbService _comicBookService;
        private readonly IUserService _userService;
        private readonly ILogger<ComicBooksController> _logger;
        private readonly IUriService _uriService;

        public ComicBooksController(IComicBookDbService comicBookService, IUserService userService,
            ILogger<ComicBooksController> logger, IUriService uriService)
        {
            _comicBookService = comicBookService;
            _userService = userService;
            _logger = logger;
            _uriService = uriService;
        }

        /// <summary>
        /// Retrieves all comics from a logged in users collection.
        /// </summary>
        /// <remarks>
        ///     Sample Request:
        ///     GET /comicBooks
        /// </remarks>
        /// <returns>an array of comicBook's</returns>
        /// <response code="200">The entire collection of comics</response>
        [HttpGet]
        [Produces(typeof(ComicBook))]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<ActionResult<IEnumerable<ComicBook>>> GetAllComics([FromQuery] PaginationFilter filter)
        {
            try
            {
                var collectionId = await GetCollectionGuid();
                var route = Request.Path.Value;
                var validFilter = new PaginationFilter(filter.PageNumber, filter.PageSize);
                var pagedResponse =
                    await _comicBookService.GetItemsAsync(collectionId, validFilter, _uriService, route);
                return Ok(pagedResponse);
            }
            catch (Exception e)
            {
                _logger.LogError(e, e.Message);
                return Problem($"A problem occured while retrieving comics msg: {e.Message}");
            }
        }

        /// <summary>
        /// Add a comicBook to the API
        /// </summary>
        /// <remarks>
        ///    Sample Request:
        ///
        ///     POST /comicBooks
        ///     {       
        ///        "isbn": null,
        ///        "author": "Walt Disney",
        ///        "publisher": "Egmont Serieforlaget",
        ///        "seriesName": "Jumbobog",
        ///        "state": "Good",
        ///        "title": "Kongernes Sværd",
        ///        "edition": 1,
        ///        "issue": 277,
        ///        "value": 8,
        ///        "coverType": "Paperback",
        ///        "productionYear": 2003,
        ///        "remarks": null,
        ///        "thumbnailImage": null
        ///     }
        /// </remarks>
        /// <returns>The created comicBook item</returns>
        /// <response code="201">Created</response>
        /// <response code="400">Bad Request</response>
        [HttpPost]
        [Consumes(MediaTypeNames.Application.Json)]
        [Produces(MediaTypeNames.Application.Json)]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<ActionResult> Create([FromBody] ComicBook comicBook)
        {
            try
            {
                comicBook.Id = Guid.NewGuid().ToString();
                var collectionGuid = await GetCollectionGuid();
                await _comicBookService.AddItemAsync(collectionGuid, comicBook);

                return CreatedAtAction(nameof(GetById),
                    new {guid = comicBook.Id}, comicBook);
            }
            catch (Exception e)
            {
                _logger.LogError(e, e.Message);
                return BadRequest(e);
            }
        }

        /// <summary>
        /// Create a collection and tie to the logged in user
        /// </summary>
        /// <remarks>
        ///    POST /comicBooks/collections/create
        /// </remarks>
        /// <returns>the GUID id of the created collection or 200OK if collection already exists.</returns>
        /// <repsonse code="200">OK</repsonse>
        /// <response code="201">Created</response>
        /// <response code="400">Bad Request</response>
        [HttpPost("Collections/Create")]
        [Consumes(MediaTypeNames.Application.Json)]
        [Produces(MediaTypeNames.Application.Json)]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<ActionResult> CreateCollection()
        {
            try
            {
                var user = _userService.GetUser().Result as MyPreciousComicsUser;
                if ((user == null) ||
                    ((user.ComicCollectionGuid != null) && !user.ComicCollectionGuid.Equals(String.Empty)))
                    return Ok("User already has a collection!");
                var collection = await Task.FromResult(_comicBookService.CreateComicsCollectionAsync(_userService));
                if (collection == null) return BadRequest();

                var result = await Task.FromResult(await _userService.AssignRoleToUser("Collector"));
                if (result)
                {
                    _userService.RegisterUserWithCollection(collection.Result.Id, user.Id);
                    return Created(new Uri($"{Request.Path}/{collection.Id}", UriKind.Relative), collection);
                }
            }
            catch (Exception e)
            {
                _logger.LogError(e, e.Message);
                return BadRequest(e);
            }

            var err = new ErrorResponse()
            {
                Message = "Failed to create collection",
                Status = 500
            };

            return new ObjectResult(err);
        }

        private async Task<string> GetCollectionGuid() {
            if (await _userService.GetUser() is MyPreciousComicsUser user)
            {
                return user.ComicCollectionGuid;
            }

            throw new NullReferenceException("User is null!");
        }

        /// <summary>
        /// Update's a comicBook
        /// </summary>
        /// <remarks>
        ///    Sample Request:
        ///
        ///     PUT /comicBooks
        ///     {
        ///         "cartoonId": 202,
        ///         "isbn": null,
        ///         "author": "Walt Disney",
        ///         "publisher": "Egmont Serieforlaget",
        ///         "seriesName": "Jumbobog",
        ///         "state": "good+",
        ///         "title": "Anders And giver bolden op",
        ///         "edition": 1,
        ///         "issue": 46,
        ///         "value": 10,
        ///         "coverType": "Paperback",
        ///         "productionYear": 1982,
        ///         "thumbnailImage": null
        ///     }
        /// </remarks>
        /// <returns>The updated comicBook</returns>
        /// <response code="202">Accepted</response>
        /// <response code="400">Bad Request</response>
        [HttpPut]
        [Consumes(MediaTypeNames.Application.Json)]
        [Produces(MediaTypeNames.Application.Json)]
        [ProducesResponseType(StatusCodes.Status202Accepted)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<ActionResult> Update([FromBody] ComicBook comicBook)
        {
            try
            {
                var collectionGuid = await GetCollectionGuid();

                await _comicBookService.UpdateItemAsync(collectionGuid, comicBook);

                return Accepted();
            }
            catch (Exception e)
            {
                _logger.LogError($"Failed to update comic with id: {comicBook.Id} ", e);
                return BadRequest(e);
            }
        }

        /// <summary>
        /// Retrieves a comicBook by it's id
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///     GET /comicBooks/{id}     
        /// </remarks>
        /// <param name="guid"></param>
        /// <returns>a comicBook</returns>
        /// <response code="200">OK</response>
        /// <response code="404">NotFound</response>
        [HttpGet("{guid}")]
        [Produces(MediaTypeNames.Application.Json)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> GetById(string guid)
        {
            var collectionGuid = await GetCollectionGuid();
            var comicBook = await _comicBookService.GetItemAsync(collectionGuid, guid);
            if (comicBook == null)
            {
                return NotFound($"ComicBook with GUID {guid} were not found!");
            }

            return Ok(new Response<ComicBook>(comicBook));
        }

        /// <summary>
        /// retrieves a collection of cartoons given a comicBook series name.
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///     GET /comicBooks/{seriesName}     
        /// </remarks>
        /// <param name="seriesName"></param>
        /// <returns>an array of cartoons</returns>
        /// <response code="200">An entire series of cartoons</response>
        /// <response code="204">No Content</response>
        [HttpGet("series")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        public async Task<ActionResult<IEnumerable<ComicBook>>> GetBySeriesName([FromQuery] string seriesName)
        {
            var results = await _comicBookService.GetComicsAsyncBySeriesName(seriesName);
            return Ok(results);
        }

        /// <summary>
        /// retrieves cartoons matching a given series name and issue.
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///     GET /comicBooks/{seriesName}/{issue}     
        /// </remarks>
        /// <param name="seriesName"></param>
        /// <param name="issue"></param>
        /// <returns>an array of cartoons</returns>
        /// <response code="200">OK</response>
        /// <response code="204">No Content</response>
        [HttpGet("seriesandissue")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        public async Task<ActionResult<IEnumerable<ComicBook>>> GetComicSeriesIssue([FromQuery] string seriesName,
            [FromQuery] int issue)
        {
            var results = await _comicBookService.GetComicAsyncBySeriesIssue(seriesName, issue);
            return Ok(results);
        }

        /// <summary>
        /// Deletes a comicBook given an id
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///     DELETE /comicBooks/{id}   
        /// </remarks>
        /// <param name="comicGuid"></param>
        /// <returns>204 No Content</returns>
        /// <response code="204">No Content</response>
        /// <response code="404">Not Found</response>
        [HttpDelete("{comicGuid}")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> DeleteById([Bind("comicGuid")] string comicGuid)
        {
            var collectionGuid = await GetCollectionGuid();
            await _comicBookService.DeleteItemAsync(collectionGuid, comicGuid);
            return NoContent();
        }

        [HttpGet("reseed")]
        public async Task<IActionResult> ReSeedComicBooks()
        {
            var books = await System.IO.File.ReadAllTextAsync(@"cartoons.json");

            try
            {
                var comicBooks = JsonSerializer.Deserialize<List<OldComic>>(books);
                Console.WriteLine(comicBooks.Count);

                foreach (var comicBook in comicBooks)
                {
                    var guid = Guid.NewGuid().ToString();
                    var user = await _userService.GetUser() as MyPreciousComicsUser;

                    var userComicCollectionGuid = user.ComicCollectionGuid;
                    await _comicBookService.AddItemAsync(userComicCollectionGuid, new ComicBook()
                    {
                        Id = guid,
                        CollectionId = userComicCollectionGuid,
                        Author = comicBook.author,
                        Edition = comicBook.edition,
                        Isbn = comicBook.isbn,
                        Issue = comicBook.issue,
                        Publisher = comicBook.publisher,
                        Remarks = comicBook.remarks,
                        State = comicBook.state,
                        Title = comicBook.title,
                        Value = comicBook.value,
                        CoverType = comicBook.coverType,
                        ForSale = false,
                        ProductionYear = comicBook.productionYear,
                        SeriesName = comicBook.seriesName
                    });
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }


            return NoContent();
        }
    }
}