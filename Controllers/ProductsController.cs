using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using ComicBookApi.Mappers;
using ComicBookApi.Models.Products;
using ComicBookApi.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace ComicBookApi.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class ProductsController : ControllerBase
    {
        
        private readonly IUserService _userService;
        private readonly ILogger<ProductsController> _logger;
        private readonly IComicBookDbService _dbContext;

        public ProductsController(IUserService userService, ILogger<ProductsController> logger, IComicBookDbService dbContext)
        {
            _userService = userService;
            _logger = logger;
            _dbContext = dbContext;
        }
        
        /// <summary>
        /// Retrieves all products.
        /// </summary>
        /// <remarks>
        ///     Sample Request:
        ///     GET /products
        /// </remarks>
        /// <returns>an array of product's</returns>
        /// <response code="200">The entire collection of products</response>
        [HttpGet]
        [Produces(typeof(IEnumerable<ProductDto>))]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<ActionResult<IEnumerable<ProductDto>>> GetAllProducts()
        {
            try
            {
                var products = await _dbContext.GetAllProducts();
                var productDtos = ProductMapper.MapFrom(products);
                return Ok(productDtos);
            }
            catch (Exception e)
            {
                _logger.LogError(e, e.Message);
                return Problem($"A problem occured while retrieving product list - msg: {e.Message}");
            }
        }

    }
}