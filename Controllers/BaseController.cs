﻿﻿using System;
 using System.Threading.Tasks;
 using ComicBookApi.Data.Entities;
 using Microsoft.AspNetCore.Identity;
 using Microsoft.AspNetCore.Mvc;
 using Microsoft.Extensions.Logging;

 namespace ComicBookApi.Controllers
{
    [Route("api/[controller]/[action]")]
    public abstract class BaseController : Controller
    {
        private readonly UserManager<MyPreciousComicsUser> UserManager;

        protected BaseController(UserManager<MyPreciousComicsUser> userManager)
        {
            UserManager = userManager;
        }

        protected async Task<MyPreciousComicsUser> GetCurrentUser() => await UserManager.FindByIdAsync(User.Identity.Name);

        protected string GetUserId() => User.Identity.Name;

        protected IActionResult ReturnBadRequest(ILogger logger, Exception e)
        {
            logger.LogError("Get Property Failed " + e);
            return BadRequest(e.Message);
        }
    }
}
