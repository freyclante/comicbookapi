using System;
using System.Collections.Generic;
using System.IO;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;
using ComicBookApi.Configuration;
using ComicBookApi.Data.Entities;
using ComicBookApi.Data.Entities.Comics;
using ComicBookApi.Helpers;
using ComicBookApi.Services;
using ImageMagick;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;

namespace ComicBookApi.Controllers
{
    [Authorize]
    [ApiController]
    [Route("[controller]")]
    public class ImagesController : ControllerBase
    {
        private readonly IComicBookDbService _comicService;
        private readonly IUserService _userService;
        private readonly PreciousComicsStorageConfig _config;

        public ImagesController(IOptions<PreciousComicsStorageConfig>
            config, IComicBookDbService comicService, IUserService userService)
        {
            _comicService = comicService;
            _userService = userService;
            _config = config.Value;
        }

        [HttpPost("upload")]
        public async Task<IActionResult> Upload(IFormFile file)
        {
            var comicGuid = Request.Headers["Comic-Guid"];
            var imageRemark = Request.Headers["Image-Remarks"];

        Uri imageBlobUri = null;

            try
            {
                if (file == null)
                    return BadRequest("No files received from the upload");

                if (_config.AccountKey == string.Empty || _config.AccountName == string.Empty)
                    return BadRequest(
                        "sorry, can't retrieve your azure storage details from key-vault, make sure that you add azure storage details there");

                if (_config.ImageContainer == string.Empty)
                    return BadRequest("Please provide a name for your image container in the azure blob storage");

                if (StorageHelper.IsImage(file))
                {
                    var tmpFilePath = Path.Combine(Path.GetTempPath(), file.FileName);

                    await using (Stream stream = System.IO.File.Create(tmpFilePath))
                    {
                        await file.OpenReadStream().CopyToAsync(stream);
                    }
                    
                    var fileInfo = new FileInfo(tmpFilePath);
                    
                    Console.WriteLine("Bytes before: " + fileInfo.Length);

                    var optimizer = new ImageOptimizer
                    {
                        OptimalCompression = true,
                        IgnoreUnsupportedFormats = false
                    };
                    optimizer.Compress(fileInfo);

                    fileInfo.Refresh();
                    Console.WriteLine("Bytes after:  " + fileInfo.Length);

                    if (file.Length > 0)
                    {
                        using (Stream stream = System.IO.File.OpenRead(tmpFilePath))
                        {
                            imageBlobUri = await StorageHelper
                                .UploadFileToStorage(stream, fileInfo.Name, _config);
                        }
                    }
                }
                else
                {
                    return new UnsupportedMediaTypeResult();
                }

                if (imageBlobUri != null)
                {
                    var user = _userService.GetUser().Result as MyPreciousComicsUser;
                    if (user == null) return BadRequest("No user is logged in!");
                    
                    var collectionGuid = user.ComicCollectionGuid;
                    
                    var comicRecord = await _comicService.GetItemAsync(collectionGuid, comicGuid);
                    var imageGuid = Guid.NewGuid().ToString();
                    var comicPhoto = new ComicPhoto {Id = imageGuid, Remarks = imageRemark, Uri = imageBlobUri.ToString()};

                    comicRecord.Photos ??= new List<ComicPhoto>();
                    comicRecord.Photos.Add(comicPhoto);

                    await _comicService.UpdateItemAsync(collectionGuid, comicRecord);
                    return new AcceptedResult();
                }
                else
                    return BadRequest("The image couldn't upload to the storage");
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        // GET /api/images/{imageGuid}/thumbnails
        [HttpGet("{imageGuid}/thumbnails")]
        public async Task<IActionResult> GetThumbNails(string imageGuid)
        {
            try
            {
                if (_config.AccountKey == string.Empty || _config.AccountName == string.Empty)
                    return BadRequest(
                        "Sorry, can't retrieve your Azure storage details from key-vault, make sure that you add Azure storage details there.");

                if (_config.ImageContainer == string.Empty)
                    return BadRequest("Please provide a name for your image container in Azure blob storage.");

                var thumbnailUrls = await StorageHelper.GetThumbNailUrls(_config);
                return new ObjectResult(thumbnailUrls);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}