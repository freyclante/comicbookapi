using Azure.Storage;
using Azure.Storage.Blobs;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using ComicBookApi.Configuration;

namespace ComicBookApi.Helpers
{
    public class StorageHelper
    {
        public static bool IsImage(IFormFile file)
        {
            if (file.ContentType.Contains("image"))
            {
                return true;
            }

            var formats = new string[] {".jpg", ".png", ".gif", ".jpeg"};

            return formats.Any(item => file.FileName.EndsWith(item, StringComparison.OrdinalIgnoreCase));
        }

        public static async Task<Uri> UploadFileToStorage(Stream fileStream, string fileName,
            PreciousComicsStorageConfig storageConfig)
        {
            var blobUri = new Uri("https://" +
                                  storageConfig.AccountName +
                                  ".blob.core.windows.net/" +
                                  storageConfig.ImageContainer +
                                  "/" + fileName);

            var storageCredentials =
                new StorageSharedKeyCredential(storageConfig.AccountName, storageConfig.AccountKey);

            var blobClient = new BlobClient(blobUri, storageCredentials);

            await blobClient.UploadAsync(fileStream);

            return await Task.FromResult(blobUri);
        }

        public static async Task<List<string>> GetThumbNailUrls(PreciousComicsStorageConfig storageConfig)
        {
            var thumbnailUrls = new List<string>();

            var accountUri = new Uri("https://" + storageConfig.AccountName + ".blob.core.windows.net/");

            var blobServiceClient = new BlobServiceClient(accountUri);

            var container = blobServiceClient.GetBlobContainerClient(storageConfig.ThumbnailContainer);

            if (await container.ExistsAsync())
            {
                thumbnailUrls.AddRange(container.GetBlobs().Select(blobItem => container.Uri + "/" + blobItem.Name));
            }

            return await Task.FromResult(thumbnailUrls);
        }
    }
}