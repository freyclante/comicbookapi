using System;
using Microsoft.AspNetCore.Identity;

namespace ComicBookApi.Data.Entities
{
    public class MyPreciousComicsUser : IdentityUser
    {
        public DateTime Joined { get; set; }
        public string ComicCollectionGuid { get; set; }
        public string SubscriptionName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
    }
}