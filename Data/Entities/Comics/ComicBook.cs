using System.Collections.Generic;
using Newtonsoft.Json;

namespace ComicBookApi.Data.Entities.Comics
{
    public class ComicBook
    {
        [JsonProperty(PropertyName = "id")]
        public string Id { get; set; }
        [JsonProperty(PropertyName = "isbn")]
        public string Isbn { get; set; }
        [JsonProperty(PropertyName = "author")]
        public string Author { get; set; }
        [JsonProperty(PropertyName = "comicBookArtist")]
        public string ComicBookArtist { get; set; }
        [JsonProperty(PropertyName = "publisher")]
        public string Publisher { get; set; }
        [JsonProperty(PropertyName = "seriesName")]
        public string SeriesName { get; set; }
        [JsonProperty(PropertyName = "state")]
        public string State { get; set; }
        [JsonProperty(PropertyName = "title")]
        public string Title { get; set; }
        [JsonProperty(PropertyName = "edition")]
        public int? Edition { get; set; }
        [JsonProperty(PropertyName = "issue")]
        public int? Issue { get; set; }
        [JsonProperty(PropertyName = "value")]
        public double? Value { get; set; }
        [JsonProperty(PropertyName = "coverType")]
        public string CoverType { get; set; }
        [JsonProperty(PropertyName = "productionYear")]
        public int? ProductionYear { get; set; }
        [JsonProperty(PropertyName = "remarks")]
        public string Remarks { get; set; }
        [JsonProperty(PropertyName = "forSale")]
        public bool ForSale { get; set; }
        [JsonProperty(PropertyName = "photos")]
        public List<ComicPhoto> Photos { get; set; }

        [JsonProperty(PropertyName = "comicCollectionId")]
        public string CollectionId { get; set; }
        public ComicCollection ComicCollection { get; set; }
    }
}