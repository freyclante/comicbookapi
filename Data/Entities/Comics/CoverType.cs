namespace ComicBookApi.Data.Entities.Comics
{
    public class CoverType
    {
        public string Id { get; set; }
        public string Name { get; set; }
    }
}