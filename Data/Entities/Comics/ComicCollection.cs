using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace ComicBookApi.Data.Entities.Comics
{
    public class ComicCollection
    {
        [JsonProperty(PropertyName = "id")]
        public string Id { get; set; }

        [JsonProperty(PropertyName = "ownerId")]
        public string OwnerId { get; set; }

        [JsonProperty(PropertyName = "created")]
        public DateTime Created { get; set; }

        [JsonProperty(PropertyName = "comics")]
        public List<ComicBook> Comics { get; set; }

        [JsonProperty(PropertyName = "isPubliclySearchable")]
        public bool IsPubliclySearchable { get; set; }
    }
}