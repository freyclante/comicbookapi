using Newtonsoft.Json;

namespace ComicBookApi.Data.Entities.Comics
{
    public class ComicPhoto
    {
        [JsonProperty(PropertyName = "id")]
        public string Id { get; set; }
        [JsonProperty(PropertyName = "photoUri")]
        public string Uri { get; set; }
        [JsonProperty(PropertyName = "remarks")]
        public string Remarks { get; set; }
        
        [JsonProperty(PropertyName = "comicBookId")]
        public string ComicBookId { get; set; }
        public ComicBook ComicBook { get; set; }
    }
}