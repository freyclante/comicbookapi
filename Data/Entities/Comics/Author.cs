namespace ComicBookApi.Data.Entities.Comics
{
    public class Author
    {
        public string Id { get; set; }
        public string Name { get; set; }
    }
}