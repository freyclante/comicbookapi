namespace ComicBookApi.Data.Entities.Comics
{
    public class ComicState
    {
        public string Id { get; set; }
        public string State { get; set; }
        public string StateAbbreviation { get; set; }
        public double StateIndex { get; set; }
        public string Description { get; set; }
    }
}