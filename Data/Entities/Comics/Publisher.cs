namespace ComicBookApi.Data.Entities.Comics
{
    public class Publisher
    {
        public string Id { get; set; }
        public string Name { get; set; }
    }
}