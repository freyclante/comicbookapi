namespace ComicBookApi.Data.Entities.Comics
{
    public class Series
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public int LaunchYear { get; set; }
        public int? TerminationYear { get; set; }
        /*
        public bool Searchable { get; set; }
    */
    }
}