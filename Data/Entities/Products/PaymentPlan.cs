using System.Text.Json.Serialization;

namespace ComicBookApi.Data.Entities.Products
{
    public class PaymentPlan
    {
        [JsonIgnore]
        public int PaymentPlanId { get; set; }
        public bool IsRecurring { get; set; }
        public string Frequency { get; set; }

        public int  ProductId { get; set; }
        public Product Product { get; set; }
        
    }
}