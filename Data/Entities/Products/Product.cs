using System.Text.Json.Serialization;

namespace ComicBookApi.Data.Entities.Products
{
    public class Product
    {
        [JsonIgnore]
        public int ProductId  { get; set; }
        public string Name { get; set; }
        public bool IsFree { get; set; }
        public double? Price { get; set; }
        
        public string Description { get; set; }
        public PaymentPlan PaymentPlan { get; set; }
    }
}