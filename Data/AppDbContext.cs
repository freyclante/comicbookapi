using ComicBookApi.Configuration;
using ComicBookApi.Data.Entities;
using ComicBookApi.Data.Entities.Comics;
using ComicBookApi.Data.Entities.Products;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace ComicBookApi.Data
{
    public class AppDbContext : IdentityDbContext<MyPreciousComicsUser>
    {
        
        public DbSet<Product> Products { get; set; }
        public DbSet<PaymentPlan> PaymentPlans { get; set; }
        public DbSet<ComicCollection> ComicCollections { get; set; }
        public DbSet<ComicBook> ComicBooks { get; set; }
        public DbSet<ComicPhoto> ComicPhotos { get; set; }
        public AppDbContext(DbContextOptions<AppDbContext> options)
            : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            builder.Entity<MyPreciousComicsUser>(entity => { entity.ToTable(name: "Users"); });
            builder.Entity<ComicCollection>().ToTable("ComicCollections")
                .HasMany<ComicBook>()
                .WithOne(b => b.ComicCollection)
                .HasForeignKey(cb => cb.CollectionId);

            builder.Entity<ComicBook>().ToTable("Comics")
                .HasMany<ComicPhoto>()
                .WithOne(cp => cp.ComicBook)
                .HasForeignKey(cp => cp.ComicBookId);
            
            builder.Entity<IdentityRole>(entity => { entity.ToTable(name: "Roles"); });
            builder.Entity<IdentityUserRole<string>>(entity => { entity.ToTable("UserRoles"); });
            builder.Entity<IdentityUserClaim<string>>(entity => { entity.ToTable("UserClaims"); });
            builder.Entity<IdentityUserLogin<string>>(entity => { entity.ToTable("UserLogins"); });
            builder.Entity<IdentityUserToken<string>>(entity => { entity.ToTable("UserTokens"); });
            builder.Entity<IdentityRoleClaim<string>>(entity => { entity.ToTable("RoleClaims"); });
            
            builder.Entity<Product>().ToTable("Products")
                .HasOne(p => p.PaymentPlan)
                .WithOne(pp => pp.Product)
                .HasForeignKey<PaymentPlan>(pmp => pmp.ProductId);

            builder.ApplyConfiguration(new AppUserConfiguration());
        }
    }
}